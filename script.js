
let digit0 = document.querySelector("#digit0");
let digit6 = document.querySelector("#digit6");
let digit5 = document.querySelector("#digit5");
let digit4 = document.querySelector("#digit4");

let digit3 = document.querySelector("#digit3");
let digit2 = document.querySelector("#digit2");
let digit1 = document.querySelector("#digit1");

let digit9 = document.querySelector("#digit9");
let digit8 = document.querySelector("#digit8");
let digit7 = document.querySelector("#digit7");

let devide = document.querySelector("#devide");
let multiply = document.querySelector("#multiply");
let substract = document.querySelector("#substract");
let add = document.querySelector("#add");
let equals = document.querySelector("#equals");
let result = document.querySelector(".result input");
let clear = document.querySelector("#clear");

let firstNumber = 0;
let secondNumber = 0;

let isFirstNumber = true;
let operation = '';

function buildNumber(digit) {
	if (isFirstNumber) {
		firstNumber = firstNumber * 10 + digit;
		result.value += digit;
	} else {
		secondNumber = secondNumber * 10 + digit;
		result.value += digit;
	}

}
digit0.addEventListener('click', function () {
	buildNumber(0);
});
digit1.addEventListener('click', function () {
	buildNumber(1);
});

digit2.addEventListener('click', function () {
	buildNumber(2);
});

digit3.addEventListener('click', function () {
	buildNumber(3);
});

digit4.addEventListener('click', function () {
	buildNumber(4);
});

digit5.addEventListener('click', function () {
	buildNumber(5);
});

digit6.addEventListener('click', function () {
	buildNumber(6);
});

digit7.addEventListener('click', function () {
	buildNumber(7);
});

digit8.addEventListener('click', function () {
	buildNumber(8);
});

digit9.addEventListener('click', function () {
	buildNumber(9);
});

devide.addEventListener("click", function () {
	isFirstNumber = false;
	operation = '/';
	result.value += " " + operation + " ";
});

multiply.addEventListener("click", function () {
	isFirstNumber = false;
	operation = '*';
	result.value += " " + operation + " ";
});

substract.addEventListener("click", function () {
	isFirstNumber = false;
	operation = '-';
	result.value += " " + operation + " ";
});

add.addEventListener("click", function () {
	isFirstNumber = false;
	operation = '+';
	result.value += " " + operation + " ";
})

equals.addEventListener("click", function () {
	result.value += " = ";
	switch (operation) {
		case "/":
			result.value += firstNumber / secondNumber;
			break;
		case "*":
			result.value += firstNumber * secondNumber;
			break;
		case "-":
			result.value += firstNumber - secondNumber;
			break;
		case "+":
			result.value += firstNumber + secondNumber;
			break;
		default:
			alert("Nu putem evalua aceasta operatie!")
			break;
	}
	reset();
});

clear.addEventListener("click", function () {
	reset();
	result.value = "";
});

function reset() {
	firstNumber = 0;
	secondNumber = 0;
	operation = 0;
	isFirstNumber = true;
}